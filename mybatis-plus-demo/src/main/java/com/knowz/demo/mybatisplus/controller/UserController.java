package com.knowz.demo.mybatisplus.controller;

import com.knowz.demo.mybatisplus.model.User;
import com.knowz.demo.mybatisplus.service.UserService;
import jakarta.annotation.Resource;
import jakarta.websocket.server.PathParam;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/users")
@RestController
public class UserController {
    @Resource
    private UserService userService;

    @GetMapping("list")
    public List<User> list() {
        return userService.list();
    }

    @PostMapping("save")
    public boolean save(@RequestBody User user) {
        return userService.save(user);
    }

    @GetMapping("{id}")
    public User getById(@PathVariable("id") Long id) {
        return userService.getById(id);
    }
}
