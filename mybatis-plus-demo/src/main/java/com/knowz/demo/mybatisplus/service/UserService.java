package com.knowz.demo.mybatisplus.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.knowz.demo.mybatisplus.mapper.UserMapper;
import com.knowz.demo.mybatisplus.model.User;
import org.springframework.stereotype.Service;

@Service
public class UserService extends ServiceImpl<UserMapper, User> {
}