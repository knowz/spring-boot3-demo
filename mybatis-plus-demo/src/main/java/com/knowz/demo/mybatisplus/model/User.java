package com.knowz.demo.mybatisplus.model;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("t_user")
public class User {
    private Long id;
    private String name;
    private Integer age;
    private Integer status;
}
